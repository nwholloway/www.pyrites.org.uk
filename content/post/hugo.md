---
date: 2018-04-06T15:12:01+01:00
title: Switch to Hugo
tags: [ hugo ]
---

I've been a developer using web technologies for many years now, and
have an understanding of how HTML, JavaScript and CSS may be bought
together to give a visually appealing, easy to use, responsive site.
I work with people who are wizards in this area.

I, on the other hand, have no design skills.  Previous sites have been
simply been places for me to make pieces available for download, and
have largely been unstyled.

I dabbled with using [Amaya](https://www.w3.org/Amaya/) for the HTML
creation, and while it was able to aid in editing of each page, it didn't
provide enough features for me to manage an entire site.

I switched ISP in 2008, and my site vanished from the Internet along
with the associated free hosting.

The next site was hosted using my own domain name, and on a server
I control.  I decided to start the site from scratch in 2016. The
technology features are:

- HTTP/2 support
- SSL
- IPv4 and IPv6

The combination of [Caddy](https://caddyserver.com/) and
[Let's Encrypt](https://letsencrypt.org/) means that SSL is
simple.  I'm pleased to see it score `A+` on [SSL Labs server
test](https://www.ssllabs.com/ssltest/).

However, the content was lacking.  Severely lacking.  The site consisted
of a single page.

{{< highlight html >}}
<!DOCTYPE html>
<html>
<head><title>Pyrites</title></head>
<body><h1>Pyrites</h1></body>
</html>
{{< / highlight >}}

---

I have some small projects that I'd like to publish some details of,
and so it is time to add some content.

I decided to use [Hugo](https://gohugo.io/about/) as a way of generating
the site.  I don't mind if my site looks identical to others using the
same theme, I just want it to look reasonable and have a responsive
layout.

The key features are:

- Content easily authored using Markdown and Hugo shortcodes
- Hugo configuration is alongside content (in [Git](https://git-scm.com/))
- Live reload during authoring for preview
- Site generation is quick
- Theme can easily be switched if required in the future

Now I have no excuse...
