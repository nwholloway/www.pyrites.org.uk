---
date: 2021-01-03T16:02:00+00:00
title: Seagate Personal Cloud (2/2)
tags:
  - nas
  - debian
---

In [Part 1](../seagate-personal-cloud-1/) I went as far as I could remotely
probing the device from the [Debian](https://www.debian.org) installer.

----
# Remote Access

To be able to SSH in without executing the Debian installer, I set
the password on the `root` account to be `insecure`.

```
~ # echo root:NSJZGbl8TzKic::::::: >> /etc/shadow
```

You can also use `ssh-copy-id` so SSH uses your key for authentication,
rather than needing to type the password each time.

----
# Copying Back Data

The minimal installation environment does not support
`scp`, `sftp`, or `rsync`, so you have to use the available
[BusyBox](https://www.busybox.net/) versions such as `dd`, `cat`, or
`tar`.

For example to fetch back the contents of the mounted rescue partition

```
ssh -n root@192.168.1.101 \
  "cd /mnt/sda2 && tar -cf - rescue" > rescue.tar
```

----
# Rescue Partition Content

Unpacking the pulled back `product_image.tar.lzma` confirmed this looked
like a pristine installation, as the contents of `etc/issue` were:

```
NAS OS 4.3.15.1
Linux \r on an \m / \l
```

This matches the version listed in the `versions` file.

# Unpacking the uImage 

The `uImage_1.5.16-arm` file in the rescue partition is a "u-boot legacy uImage".

My assumption was that the uImage is a kernel image with an embedded
`initramfs` which is responsible for performing the re-initialisation
when required.

I found that `binwalk` enabled me to locate and extract the image.  The first
step was to extract the uncompressed kernel.

```
$ binwalk -y gzip rescue/uImage_1.5.16-arm 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
18260         0x4754          gzip compressed data, maximum compression, from Unix, last modified: 1970-01-01 00:00:00 (null date)

$ tail +18261c rescue/uImage_1.5.16-arm | gunzip -q > Image
```

From the uncompressed kernel, we can locate and uncompress the CPIO
archive of the initramfs (ignoring error when trying to decompress bytes
after data).

```
$ binwalk -y lzma Image 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
6488095       0x63001F        LZMA compressed data, properties: 0xC0, dictionary size: 0 bytes, uncompressed size: 64 bytes
6505340       0x63437C        LZMA compressed data, properties: 0x5D, dictionary size: 33554432 bytes, uncompressed size: -1 bytes

$ tail +6505341c Image | unlzma > initramfs.cpio
unlzma: (stdin): Compressed data is corrupt
```

Finally, I unpacked the archive to be able to inspect the contents.

```
$ mkdir initramfs && cpio -D initramfs -i < initramfs.cpio
cpio: dev/zero: Cannot mknod: Operation not permitted
cpio: dev/null: Cannot mknod: Operation not permitted
cpio: dev/tty: Cannot mknod: Operation not permitted
cpio: dev/console: Cannot mknod: Operation not permitted
181173 blocks
```

----
# Examining Initramfs

There are some symbolic links with absolute paths in the archive.
The following snippet converts them to relative links to ease examination.

```
$ find initramfs -lname "/*" | while read link; do ln -vsfr initramfs$(readlink "$link") "$link"; done
'initramfs/etc/nexus.map' -> 'configs/nexus-tools/cumulus/nexus.map'
'initramfs/etc/inittab' -> 'platform/initramfs_skeleton/n090103/inittab'
'initramfs/etc/mtab' -> '../proc/mounts'
'initramfs/etc/sd_alias.conf' -> 'platform/nas-tools/n090103/sd_alias.conf'
'initramfs/etc/sysctl.d/vm.conf' -> '../platform/initramfs_skeleton/n090103/vm.conf'
'initramfs/etc/ethtool.conf' -> 'platform/ethtool/default/ethtool.conf'
'initramfs/init' -> 'bin/busybox'
```

The examination confirmed that this initramfs is responsible for restoring
the contents of the hard drive from the `product_image.tar.lzma` from
the rescue partition.

The main entry point (called indirectly by `init`) is `sbin/flash`, and there are
many utility shell functions in `lib/*`.

There are signs of this recovery process being able to be launched from
other media than the hard driver (USB?).  Due to lack of understanding of
how components like "klaxon" work, I've not investigated this possibility
further.

----
# Saving Recovery Image

The directory `etc/configs/rescue_tools/cumulus` has the file [`rescue.conf`](rescue.conf)
which includes the following:

```
# This is the rescue's partition. It shall NOT touch theses.
partition_rescue="grub_core;boot_rescue;nv_data"
```

This is used by `sbin/flash` and `lib/rescue_common` to create and
format all of the partitions apart from those specified.

So I think just saving the first 3 partitions (grub_core, boot_rescue
and nv_data) should suffice.

The partition table shows that the first 399,360 sectors is all I need.
```
~ # fdisk -l /dev/sda
Disk /dev/sda: 3.7 TiB, 4000787030016 bytes, 7814037168 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: gpt
Disk identifier: C6009DE4-B762-437A-AD70-4CB0704CDDE1

Device       Start        End    Sectors  Size Type
/dev/sda1     2048       4095       2048    1M BIOS boot
/dev/sda2     4096     397311     393216  192M Linux filesystem
/dev/sda3   397312     399359       2048    1M Linux filesystem
/dev/sda4   399360    3545087    3145728  1.5G Linux RAID
/dev/sda5  3545088    6690815    3145728  1.5G Linux RAID
/dev/sda6  6690816    8787967    2097152    1G Linux RAID
/dev/sda7  8787968    9836543    1048576  512M Linux RAID
/dev/sda8  9836544 7814035455 7804198912  3.6T Linux RAID
```

I then saved the sectors to a 195M image file.

```
ssh -n root@192.168.1.142 \
  dd if=/dev/sda bs=512 count=399360 > sda_0-399359.img
```

Along with the extracted `initramfs.cpio`, I shall save this on S3 in case I ever
decide to revert to stock firmware.

----
# Re-Install of Seagate NAS Firmware

To see how the reset process works, I powered up while holding the reset
button in.

After a period of time, I was able to configure the device from
scratch and it had reverted to version 4.3.15.1.  I launched the Debian
installer again, and was able to verify that the "root_1", "root_2",
"var" partitions had been recreated.  The "user_data" partition was
still present, but previous contents moved into `Recovery` sub-directory.

I believe that to restore a drive to stock firmware would just require
the overwriting of the start of the disk with the image saved above,
restoring the setting of `bootcmd` to be `run nexus_boot`, and then
triggering a reset.
