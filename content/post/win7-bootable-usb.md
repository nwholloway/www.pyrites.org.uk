---
date: 2021-12-27T15:00:00+00:00
title: Windows 7 ISO as Bootable USB
description: >
  How I solved the problem of making a bootable Windows 7 USB using readily
  available Linux tools
tags: [ grub2 ]
---

Although Windows 7 is long out of support, a friend asked me to assist in
recovering his laptop OS, so I put aside my preferences and prejudices to see
what I could do to help.

I found many sets of instructions about how to make a bootable USB after
copying across the contents of the ISO.  However, even the tools that were
Linux based were not readily installed on a modern distribution.

I finally realised that [Grub](https://www.gnu.org/software/grub/) was all I
required, and the following is a walk-through of the steps taken.

# USB Drive Configuration

The drive needs to have sufficient capacity to hold the contents of the Windows
7 ISO (about 8 GiB).

Replace `sdX` below with the device associated with the USB drive.

## Format

The first step is to format the USB drive.  The drive is formatted with an
MSDOS partition table, with a single NTFS formatted partition.  The label for
the NTFS partition is `W7SP1PRO64` (this will be used later).

Partition USB:

{{< highlight bash >}}
$ sudo sfdisk -d /dev/sdX <<EOF
label: dos

size=7.5GiB, type=7
EOF
{{< / highlight >}}

Format partiton:

{{< highlight bash >}}
$ sudo mkfs.ntfs --fast -L W7SP1PRO64 /dev/sdX1
{{< / highlight >}}

## Copy ISO Content

Mount the ISO image to copy from (replace `win7.iso` with the name of your ISO file):

{{< highlight bash >}}
$ sudo mount -r -o X-mount.mkdir win7.iso /tmp/win7-iso
{{< / highlight >}}

Mount the USB partition to copy to:

{{< highlight bash >}}
$ sudo mount -o X-mount.mkdir /dev/sdX1 /tmp/win7-usb
{{< / highlight >}}

Copy ISO contents to USB:

{{< highlight bash >}}
$ sudo cp -v -r /tmp/win7-iso/* /tmp/win7-usb
$ sync
{{< / highlight >}}

The `sync` may take a long time, dependent on the speed of your USB port and driver.

## Make USB Bootable

Install Grub onto the USB drive:

{{< highlight bash >}}
sudo grub-install \
  --target=i386-pc \
  --install-modules="normal search ntfs ntldr" \
  --locales=uk \
  --boot-directory=/tmp/win7-usb \
  /dev/sdX
{{< / highlight >}}

Create the file `/tmp/win7-usb/grub/grub.cfg` (the label must match the label
of the NTFS filesystem created above):

{{< highlight text >}}
insmod ntfs
search --no-floppy --label W7SP1PRO64 --set root
ntldr /bootmgr
boot
{{< / highlight >}}

# Conclusion

Unmount all of the drives, and arrange to boot from the USB drive.  If all has gone
well, you'll see the Windows 7 installer.
